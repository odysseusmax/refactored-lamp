import time
import datetime

from config import Config


class Database:
    
    def __init__(self, client):
        self.DB = client[Config.SESSION_NAME]
        self.users = self.DB.users
        self.files = self.DB.files
    
    
    def _new_user(self, id):
        return dict(
            id = id,
            join_date = datetime.date.today().isoformat(),
            last_msg_on = time.time()-60
        )
    
    
    async def _get_user(self, id):
        user = await self.users.find_one({'id':int(id)})
        return user
    
    
    async def total_users(self):
        users = await self.users.count_documents({})
        return users
    
    
    async def user_exists(self, id):
        user = await self._get_user(id)
        return True if user else False
    
    
    async def add_user(self, id):
        user = self._new_user(id)
        await self.users.insert_one(user)
    
    
    async def get_last_msg_on(self, id):
        user = await self._get_user(id)
        return user.get('last_msg_on', time.time())
    
    
    async def update_last_msg_on(self, id):
        await self.users.update_one({'id': id}, {'$set': {'last_msg_on': time.time()}})
    
    
    def _new_file(self, video_id, itag, file_id):
        return dict(
            video_id=video_id,
            itag=itag,
            file_id=file_id
        )
    
    
    async def _get_file(self, video_id, itag):
        file = await self.files.find_one({'video_id':video_id, 'itag':itag})
        return file
    
    
    async def file_exists(self, video_id, itag):
        file = await self._get_file(video_id, itag)
        return True if file else False
    
    
    async def add_file(self, video_id, itag, file_id):
        file = self._new_file(video_id, itag, file_id)
        await self.files.insert_one(file)
    
    
    async def get_file_id(self, video_id, itag):
        file = await self._get_file(video_id, itag)
        return file.get('file_id', None)
    
    
    async def delete_file(video_id, itag):
        await self.files.delete_many({'video_id': video_id, 'itag':itag})
    
