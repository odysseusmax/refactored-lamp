import urllib.parse
import traceback
import datetime
import asyncio
import time

from pyrogram import Client, Filters

from config import Config
from bot import yt, db
from bot.downloader import Downloader
from bot.utils import embed_caption, dl_progress, ul_progress, flood_shield


video_dl_filter = Filters.create(lambda _, query: query.data.startswith('fvid'))


@Client.on_callback_query(video_dl_filter)          #   Show contents of a folder
async def _(c, q):
    
    if not await db.user_exists(q.from_user.id):
        await db.add_user(q.from_user.id)
        
    last_msg_on = await db.get_last_msg_on(q.from_user.id)
    if (time.time() - last_msg_on) < 60:
        await q.answer(f"Too many requests, try after {60 - int(time.time() - last_msg_on)}s.", show_alert=True)
        return
    
    asyncio.create_task(video_process(c, q))


async def video_process(c, q):
    
    try:
        _, video_id, itag, then = q.data.split('+')
        then = int(then)
    except:
        _, video_id, itag = q.data.split('+')
        then = int(time.time()) - 121
    try:
        if int(time.time())-then > 120:
            await flood_shield(q.edit_message_text("Request expired! Please try again"))
            return
        
        await db.update_last_msg_on(q.from_user.id)
        s = time.time()
        video_data = await yt.get_video_details(f"http://youtu.be/{video_id}")
    
        if await db.file_exists(video_id, itag):
            file_id = await db.get_file_id(video_id, itag)
            if file_id:
                print('using cached')
                
                l = await flood_shield(c.send_message(Config.LOG_CHANNEL, f"For User: [{q.from_user.first_name}](tg://user?id={q.from_user.id})\n\nLink: http://youtu.be/{video_id}"))
                
                await flood_shield(q.edit_message_text("Now Uploading..."))
                
                caption = f"**{video_data.get('title')}**\n\nWatch on YouTube: https://www.youtube.com/watch?v={video_id}\nWatch on Invidious: https://invidio.us/watch?v={video_id}"
                
                await flood_shield(
                    c.send_video(
                        chat_id=q.from_user.id,
                        video=file_id,
                        caption=caption[:1024]
                    )
                )
                await flood_shield(q.edit_message_text(f"File uploaded!\n\nCompleted in `{datetime.timedelta(seconds=int(time.time()-s))}`"))
                await flood_shield(l.reply_text(f"Using Cached\n\nCompleted in `{datetime.timedelta(seconds=int(time.time()-s))}`"))
                
                return
            await db.delete_file(video_id, itag)
        print('new download')
        if video_data.get('error'):
            yt.pop_from_cache(video_id)
            await flood_shield(q.edit_message_text(video_data.get('msg')))
            
            l = await flood_shield(c.send_message(Config.LOG_CHANNEL, f"For User: [{q.from_user.first_name}](tg://user?id={q.from_user.id})\n\nLink: http://youtu.be/{video_id}"))
            await flood_shield(l.reply_text(video_data.get('msg') + '\n\n' + video_data.get('traceback', '')))
            return
        
        all_streams = video_data.get('formatStreams')
        for stream in all_streams:
            if stream.get('itag') == itag:
                video = stream
        
        stream_size = int(video.get('clen',2000*(1024**2)))
        if stream_size/(1024**2) > 2000:
            await q.answer("This cannot be uploaded.", show_alert=True)
            return
        
        await q.answer("Downloading...")
        l = await flood_shield(c.send_message(Config.LOG_CHANNEL, f"For User: [{q.from_user.first_name}](tg://user?id={q.from_user.id})\n\nLink: http://youtu.be/{video_id}"))
        log = await flood_shield(l.reply_text("Trying to download"))
        await flood_shield(q.edit_message_text("Downloading..."))
        
        video_url = video.get('url')
        thumb_url = f"https://i.ytimg.com/vi/{video_id}/mqdefault.jpg"
        
        v_dl = Downloader()
        v_dl_sts = await v_dl.download(
            video_url,
            f"{video_id}-video.mp4",
            dl_progress,
            "video",
            q
        )
        if v_dl_sts['error']:
            yt.pop_from_cache(video_id)
            await flood_shield(log.edit_text(v_dl_sts['msg']))
            await flood_shield(q.edit_message_text(v_dl_sts['msg']))
            return
        
        video_file = v_dl_sts['path']
        captions = video_data.get('captions')
        #print(captions)
        
        if captions:
            caption_files = []
            for caption in captions:
                c_dl = Downloader('text/vtt')
                caption_dl_link = urllib.parse.urljoin('https://invidio.us', caption.get('url'))
                #print(caption_dl_link)
                c_dl_sts = await c_dl.download(caption_dl_link, f"{video_id}-{caption['languageCode']}.vtt")
                #print(c_dl_sts)
                if not c_dl_sts['error']:
                    caption_files.append({
                        'label':caption.get('label'),
                        'path' :c_dl_sts['path']
                    })
            filename = f"{video_id}-{itag}.mkv"
            sts, video_file = await embed_caption(video_file, caption_files, filename)
            if not sts:
                await flood_shield(q.edit_message_text("Failed to combile captions."))
                await flood_shield(log.edit_text(video_file))
                return
        
        t_dl = Downloader()
        t_dl_sts = await t_dl.download(thumb_url, f"{video_id}-mqdefault.jpg")
        thumb_file = t_dl_sts['path']
        
        await flood_shield(q.edit_message_text("Now Uploading..."))
        await flood_shield(log.edit_text("Now Uploading..."))
        
        caption = f"**{video_data.get('title')}**\n\nWatch on YouTube: https://www.youtube.com/watch?v={video_id}\nWatch on Invidious: https://invidio.us/watch?v={video_id}"
        
        v = await flood_shield(
                c.send_video(
                    chat_id=q.from_user.id,
                    video=str(video_file),
                    caption=caption[:1024],
                    duration=video_data.get('lengthSeconds', 0),
                    thumb=str(thumb_file),
                    supports_streaming=True,
                    progress=ul_progress,
                    progress_args=(q, time.time(), "upload_video")
                )
            )
        await flood_shield(v.forward(Config.UPLOADS_CHANNEL))
        
        await db.add_file(video_id, itag, v.video.file_id)
        
        await flood_shield(q.edit_message_text(f"File uploaded!\n\nCompleted in `{datetime.timedelta(seconds=int(time.time()-s))}`"))
        await flood_shield(log.edit_text(f"File uploaded!\n\nCompleted in `{datetime.timedelta(seconds=int(time.time()-s))}`"))
        
        video_file.unlink()
        thumb_file.unlink()
        
    except:
        traceback.print_exc()
        l = await flood_shield(c.send_message(Config.LOG_CHANNEL, f"For User: [{q.from_user.first_name}](tg://user?id={q.from_user.id})\n\nLink: http://youtu.be/{video_id}"))
        await flood_shield(l.reply_text(f"Download_video \n\n{traceback.format_exc()}"))
        
