import re
import time
import datetime

from pyrogram import Client, Filters, InlineKeyboardMarkup, InlineKeyboardButton

from bot import yt, db
from bot.utils import human_bytes
from config import Config


regex = r'(http[s]*://(?:\S+\.)*(?:youtu\.be|youtube\.com)/.+)'


def yt_filter_func(_, m):
    
    if not m.text:
        return False

    r = re.findall(regex, m.text)
    if r:
        return True
    
    return False


def get_reply_text(video_data):
    title = video_data.get('title')
    viewCount = video_data.get('viewCount', 0)
    likeCount = video_data.get('likeCount', 0)
    dislikeCount = video_data.get('dislikeCount', 0)
    lengthSeconds = int(video_data.get('lengthSeconds', 0))
    lengthSecondsHuman = datetime.timedelta(seconds=lengthSeconds)
    
    text = f'**{title}**\n\nDuration: {lengthSecondsHuman} ({lengthSeconds}s)\n\n👀 {viewCount} | 👍 {likeCount} | 👎 {dislikeCount}'
    
    return text


def map_btn(video_data):
    all_streams = video_data.get('adaptiveFormats')
    video_id = video_data.get('videoId')
    now = int(time.time())
    if not all_streams:
        all_streams = video_data.get('formatStreams')
        video_btns = []
        for video in all_streams:
            res = video.get('resolution') or video.get('qualityLabel')
            leng = human_bytes(int(video.get('clen', 0)))
            itag = video.get('itag')
            video_btns.append(
                [InlineKeyboardButton(f"🎞️ {res} - {leng}", f"fvid+{video_id}+{itag}+{now}")]
            )
        return InlineKeyboardMarkup(video_btns)
    
    mp4_streams = []
    audio_streams = []
    for stream in all_streams:
        stream_type = stream.get('type')
        if 'video' in stream_type:
            res = stream.get('resolution') or stream.get('qualityLabel')
            if res is None:
                continue
            if stream.get('clen') == '0':
                continue
            mp4_streams.append(stream)
        else:
            audio_streams.append(stream)
    audio_streams = sorted(audio_streams, key=lambda k : int(k['bitrate']))
    best_audio = audio_streams[-1]
    
    mp4_stream_striped = {}
    for stream in mp4_streams:
        res = int(stream.get('resolution').replace('p', ''))
        fps = int(stream.get('fps')) or 30
        if mp4_stream_striped.get(res) is None:
            mp4_stream_striped[res] = {}
            mp4_stream_striped[res][fps] = stream
            continue
        this_stream = mp4_stream_striped.get(res)
        if this_stream.get(fps) is None:
            mp4_stream_striped[res][fps] = stream
            continue
        cur_stream = this_stream.get(fps)
        cur_stream_clen = int(cur_stream.get('clen'))
        stream_clen = int(stream.get('clen'))
        if stream_clen < cur_stream_clen:
            mp4_stream_striped[res][fps] = stream
    mp4_streams = []
    for k in mp4_stream_striped.keys():
        for j in mp4_stream_striped[k].keys():
            mp4_streams.append(mp4_stream_striped[k][j])
    
    video_btns = []
    for video in mp4_streams:
        res = video.get('resolution') or video.get('qualityLabel')
        leng = human_bytes(int(video.get('clen', 0)) + int(best_audio.get('clen')))
        itag = video.get('itag')
        fps = video.get('fps') or 30
        #print(res, itag, fps)
        video_btns.append(
            [InlineKeyboardButton(f"🎞️ {res} {fps}fps - {leng}", f"vid+{video_id}+{itag}+{now}")]
        )
    
    audio_btns = []
    for i, audio in enumerate(audio_streams):
        #print(i, len(audio_streams), i==len(audio_streams))
        res, _ = human_bytes(int(audio.get('bitrate')), True)
        res = int(res)
        leng = human_bytes(int(audio.get('clen')))
        itag = audio.get('itag')
        audio_btns.append(
            [InlineKeyboardButton(f"🎵 {res}kbps - {leng}", f"aud+{video_id}+{itag}+{now}")]
        )
    return InlineKeyboardMarkup(video_btns + audio_btns)
    
    

@Client.on_message(Filters.create(yt_filter_func) & Filters.incoming & Filters.private)
async def _(c, m):
    
    if not await db.user_exists(m.chat.id):
        await db.add_user(m.chat.id)
    
    last_msg_on = await db.get_last_msg_on(m.chat.id)
    if (time.time() - last_msg_on) < 60:
        await m.reply_text(f"Too many requests, try after {60 - int(time.time() - last_msg_on)}s.", quote=True)
        return
        
    yt_link = re.findall(regex, m.text)[0]
    video_data = await yt.get_video_details(yt_link)
    #print(video_data)
    if video_data.get('error'):
        l = await m.forward(Config.LOG_CHANNEL)
        await l.reply_text(video_data.get('msg') + '\n\n' + video_data.get('traceback', ''), quote=True)
        
        await m.reply_text(video_data.get('msg'), quote=True)
        return
    
    thumbnails = video_data['videoThumbnails']
    text = get_reply_text(video_data)
    reply_btn = map_btn(video_data)
    for thumbnail in thumbnails:
        try:
            thumbnail_url = thumbnail['url']
            await m.reply_photo(
                thumbnail_url,
                caption=text,
                quote=True,
                reply_markup=reply_btn
            )
            return
        except:
            pass
    
    await m.reply_text(
        text=text,
        quote=True,
        reply_markup=reply_btn
    )

