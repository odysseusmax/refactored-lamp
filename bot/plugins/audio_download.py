import traceback
import datetime
import asyncio
import time

from pyrogram import Client, Filters

from config import Config
from bot import yt, db
from bot.downloader import Downloader
from bot.utils import convert_to_mp3, dl_progress, ul_progress, flood_shield

audio_dl_filter = Filters.create(lambda _, query: query.data.startswith('aud'))


@Client.on_callback_query(audio_dl_filter)          #   Show contents of a folder
async def _(c, q):
    
    if not await db.user_exists(q.from_user.id):
        await db.add_user(q.from_user.id)
    last_msg_on = await db.get_last_msg_on(q.from_user.id)
    if (time.time() - last_msg_on) < 60:
        await q.answer(f"Too many requests, try after {60 - int(time.time() - last_msg_on)}s.", show_alert=True)
        return
    
    asyncio.create_task(audio_process(c, q))


async def audio_process(c, q):
    
    try:
        _, video_id, itag, then = q.data.split('+')
        then = int(then)
    except:
        _, video_id, itag = q.data.split('+')
        then = int(time.time()) - 121
    try:
        
        if int(time.time())-then > 120:
            await flood_shield(q.edit_message_text("Request expired! Please try again"))
            return
        
        await db.update_last_msg_on(q.from_user.id)
        s = time.time()
        video_data = await yt.get_video_details(f"http://youtu.be/{video_id}")
        
        if await db.file_exists(video_id, itag):
            file_id = await db.get_file_id(video_id, itag)
            if file_id:
                print('using cached')
                l = await flood_shield(c.send_message(Config.LOG_CHANNEL, f"For User: [{q.from_user.first_name}](tg://user?id={q.from_user.id})\n\nLink: http://youtu.be/{video_id}"))
                
                await flood_shield(q.edit_message_text("Now Uploading..."))
                
                caption = f"**{video_data.get('title')}**\n\nWatch on YouTube: https://www.youtube.com/watch?v={video_id}\nWatch on Invidious: https://invidio.us/watch?v={video_id}"
                
                await flood_shield(
                    await c.send_audio(
                        chat_id=q.from_user.id,
                        audio=file_id,
                        caption=caption[:1024]
                    )
                )
                await flood_shield(q.edit_message_text(f"File uploaded!\n\nCompleted in `{datetime.timedelta(seconds=int(time.time()-s))}`"))
                await flood_shield(l.reply_text(f"Using Cached\n\nCompleted in `{datetime.timedelta(seconds=int(time.time()-s))}`"))
                return
            await db.delete_file(video_id, itag)
        print('new download')
        if video_data.get('error'):
            yt.pop_from_cache(video_id)
            await flood_shield(q.edit_message_text(video_data.get('msg')))
            l = await flood_shield(c.send_message(Config.LOG_CHANNEL, f"For User: [{q.from_user.first_name}](tg://user?id={q.from_user.id})\n\nLink: http://youtu.be/{video_id}"))
            await flood_shield(l.reply_text(video_data.get('msg') + '\n\n' + video_data.get('traceback', '')))
            return
        
        all_streams = video_data.get('adaptiveFormats')
        for stream in all_streams:
            if stream.get('itag') == itag:
                audio = stream
        
        if int(audio.get('clen',2000*1024*1024))/(1024**2) > 2000:
            await q.answer("This cannot be uploaded.", show_alert=True)
            return
        
        await q.answer("Downloading...")
        l = await flood_shield(c.send_message(Config.LOG_CHANNEL, f"For User: [{q.from_user.first_name}](tg://user?id={q.from_user.id})\n\nLink: http://youtu.be/{video_id}"))
        log = await flood_shield(l.reply_text("Trying to download"))
        await flood_shield(q.edit_message_text("Downloading..."))
        
        audio_url = audio.get('url')
        #print(audio_url)
        a_dl = Downloader()
        a_dl_sts = await a_dl.download(
            audio_url,
            f"{video_id}.mp3",
            dl_progress,
            "audio",
            q,
        )
        if a_dl_sts['error']:
            yt.pop_from_cache(video_id)
            await flood_shield(log.edit_text(a_dl_sts['msg']))
            await flood_shield(q.edit_message_text(a_dl_sts['msg']))
            return
        
        thumb_url = f"https://i.ytimg.com/vi/{video_id}/mqdefault.jpg"
        t_dl = Downloader()
        t_dl_sts = await t_dl.download(thumb_url, f"{video_id}-mqdefault.jpg")
        
        await flood_shield(log.edit_text("Downloaded! Now converting..."))
        await flood_shield(q.edit_message_text("Downloaded! Now converting..."))
        
        audio_file = await convert_to_mp3(a_dl_sts['path'], f"{video_id}-{itag}.mp3")
        thumb_file = t_dl_sts.get('path')

        await flood_shield(q.edit_message_text("Now Uploading..."))
        await flood_shield(log.edit_text("Now Uploading..."))
        
        caption = f"**{video_data.get('title')}**\n\nWatch on YouTube: https://www.youtube.com/watch?v={video_id}\nWatch on Invidious: https://invidio.us/watch?v={video_id}"
        
        v = await flood_shield(
                c.send_audio(
                    q.from_user.id,
                    audio=str(audio_file),
                    caption=caption[:1024],
                    duration=video_data.get('lengthSeconds', 0),
                    performer=video_data.get('author', None),
                    title=video_data.get('title', None),
                    thumb=str(thumb_file),
                    progress=ul_progress,
                    progress_args=(q, time.time(), "upload_audio")
                )
            )
        
        await flood_shield(v.forward(Config.UPLOADS_CHANNEL))
        
        await db.add_file(video_id, itag, v.audio.file_id)
        
        await flood_shield(q.edit_message_text(f"File uploaded!\n\nCompleted in `{datetime.timedelta(seconds=int(time.time()-s))}`"))
        await flood_shield(log.edit_text(f"File uploaded!\n\nCompleted in `{datetime.timedelta(seconds=int(time.time()-s))}`"))
        
        audio_file.unlink()
        thumb_file.unlink()
        
    except:
        traceback.print_exc()
        l = await flood_shield(c.send_message(Config.LOG_CHANNEL, f"For User: [{q.from_user.first_name}](tg://user?id={q.from_user.id})\n\nLink: http://youtu.be/{video_id}"))
        await flood_shield(l.reply_text(f"Download_audio \n\n{traceback.format_exc()}"))
    
