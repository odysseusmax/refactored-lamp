from pyrogram import Client, Filters

from bot import db
from config import Config


@Client.on_message(Filters.private & Filters.user(Config.AUTH_USERS) & Filters.command("status"))
async def status(client, message):
    total_count = await db.total_users()
    await message.reply_text(text = f"Total users: {total_count}", quote=True)
