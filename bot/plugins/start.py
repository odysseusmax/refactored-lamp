from pyrogram import Client, Filters

from bot import db


@Client.on_message(Filters.command("start") & Filters.incoming & Filters.private)
async def start(c, m):
    
    if not await db.user_exists(m.chat.id):
        await db.add_user(m.chat.id)
    
    await m.reply_text("Hi I'm a Youtube Media Downloader. Send any Youtube video URL.")

