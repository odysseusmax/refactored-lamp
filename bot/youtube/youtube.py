import re
import random
import datetime
import traceback

import aiohttp
import yarl


class Youtube:
    
    _VIDEO_ID_REGEX = 'http[s]*://(?:\S*\.)*(?:youtu\.be|youtube\.com)(?:/embed/|/v/|/watch\?v=|/user/\S+|/ytscreeningroom\?v=|/sandalsResorts#\w/\w/.*/|/)([^/&]{10,12})'
    _INVIDIOUS_BASE_API_URL = "https://invidio.us/api/v1/"
    
    
    def __init__(self):
        self._sesssion = aiohttp.ClientSession()
        self.mirrors = None
        self._mirrors = [
            'https://invidious.snopyta.org/',
            'https://invidio.us/',
            'https://invidious.fdn.fr',
            'https://invidious.ggc-project.de',
            'https://invidious.13ad.de/',
            'https://invidious.toot.koeln',
            'https://invidiou.site',
            'https://yt.iswleuven.be',
            'https://invidious.tube',
            'https://invidious.site/',
            'https://vid.mint.lgbt',
        ]
        self._cache = {}
    
    
    def _get_video_id(self, url):
        match = re.findall(
            self._VIDEO_ID_REGEX,
            url
        )
        if match:
            return match[0]
        
        return None
    
    
    def video_id_expired(self, video_id):
        return datetime.datetime.now() > self._cache[video_id]['added_at'] + datetime.timedelta(hours=3)
    
    
    async def _req(self, req_type, **kwargs):
        return await self._sesssion.request(
            req_type,
            **kwargs
        )
    
    
    async def _get_mirrors(self):
        if not self.mirrors:
            instances_url = 'https://instances.invidio.us/instances.json'
            r = await self._req(
                'GET',
                url = instances_url
            )
            jsn = await r.json()
            https_instances = []
            for instance in jsn:
                uri = yarl.URL(instance[1].get('uri'))
                typ = instance[1].get('type')
                if 'http' in typ:
                    try:
                        r = await self._req(
                            'GET',
                            url = uri
                        )
                        if r.status != 200:
                            continue
                    except:
                        continue
                    https_instances.append(uri)
            self.mirrors = https_instances
        
        if not self.mirrors:
            self.mirrors = self._mirrors
        

    async def _invidious_api_call(self, endpoint):
        try:
            await self._get_mirrors()
            error = None
            error_text = ''
            for instance_uri in self.mirrors:
                r = await self._req(
                    'GET',
                    url=yarl.URL(instance_uri).join(yarl.URL(endpoint))
                )
                if r.status == 200:
                    res = await r.json()
                    error = res.get('error')
                    if error is None:
                        return res
                    error_text += f"`{instance_uri}` : {error}\n\n"
                else:
                    error_text += f"`{instance_uri}` : returned status code {r.status}\n\n"
            return error_text
        except:
            traceback.print_exc()
            return traceback.format_exc()
    
    
    def pop_from_cache(self, video_id):
        if self._cache.get(video_id):
            self._cache.pop(video_id)
    
    
    async def get_video_details(self, yt_url):
        video_id = self._get_video_id(yt_url)
        if video_id is None:
            return {
                'error' : True,
                'msg' : "Not a valid Youtube video url"
            }
        
        if video_id in self._cache:
            if not self.video_id_expired(video_id):
                return self._cache[video_id]['data']
        
        video_data = await self._invidious_api_call(f"/api/v1/videos/{video_id}")
        if isinstance(video_data, str):
            return {
                'error' : True,
                'msg' : "Sorry failed to retrive video details! Try again after some time.",
                'traceback': video_data
            }
        self._cache[video_id] = {}
        self._cache[video_id]['data'] = video_data
        self._cache[video_id]['added_at'] = datetime.datetime.now()
        return video_data
