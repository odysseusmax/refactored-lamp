import asyncio
import traceback
import uuid
import os
import time
from pathlib import Path

import aiohttp
import aiofiles


class Downloader:
    
    def __init__(self, CT=None):
        self._num_of_dl_threads = 4
        self._timeout = aiohttp.ClientTimeout(total=2700, connect=None,sock_connect=None, sock_read=None)
        self._headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux armv7l) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36'
        }
        self._session = aiohttp.ClientSession(
            timeout = self._timeout,
            headers = self._headers
        )
        self._CT = CT
        self._cur = 0
        self._download_dir = Path('download/' + str(uuid.uuid4()))
        if not self._download_dir.exists():
            os.makedirs(self._download_dir)
    
    
    async def download(self, url, filename, progress=None, *args):
        try:
            self._progress = progress
            self._args = args
            
            self.url = url
            self._filename = filename
            self.response = await self._session.head(self.url, allow_redirects=True)
            #print(self.response.headers)
            if self.response.status > 400:
                await self._session.close()
                return {
                    'error' : True,
                    'msg' : f"Sorry! Download failed due to unsuccessfull response status."
                }
            ct = self._CT or self.response.headers.get('Content-Type')
            print(ct)
            if (ct is None) or ('text/html' in ct.lower()):
                await self._session.close()
                return {
                    'error' : True,
                    'msg' : "Sorry! Download failed due to unknown content type."
                }
            self._size = int(self.response.headers.get('content-length', 0))
            self._start_time = time.time()
            rslt = await self.download_step_1()
            print(rslt)
            await self._session.close()
            if rslt is None:
                dt = {
                    'error' : True,
                    'msg' : self.error,
                }
                if hasattr(self, 'traceback'):
                    dt.update({'traceback':self.traceback})
                return dt
            return {
                'error' : False,
                'path' : rslt
            }
        except:
            traceback.print_exc()
            await self._session.close()
            return {
                'error' : True,
                'msg' : "Sorry! Download failed. Try again.",
                'traceback' : traceback.format_exc()
            }
    
    
    async def range_request_supported(self):
        headers = {"Range": "bytes=0-5"}
        r = await self._session.get(self.url, headers=headers)
        size = int(r.headers.get('content-length', 0))
        return self._size != size
    
    
    async def download_step_1(self):
        try:
            #if "accept-ranges" in self.response.headers:
            if await self.range_request_supported():
                part_size = int(self._size / self._num_of_dl_threads)
                start_byte = 0
                end_byte = part_size
                aws = []
                for i in range(self._num_of_dl_threads):
                    part_file_name = self._filename + f'.part{i}'
                    aws.append(
                        self.download_step_2(part_file_name, start_byte, end_byte)
                    )
                    start_byte = end_byte + 1
                    end_byte = self._size if i+1 == self._num_of_dl_threads else end_byte + part_size 
                dl_rslt = await asyncio.gather(*aws)
                for i in dl_rslt:
                    if i is None:
                        return None
                return await self.make_file()
            else:
                return await self.download_step_2(self._filename)
        
        except Exception as e:
            traceback.print_exc()
            self.error = str(e)
            self.traceback = traceback.format_exc()
            return None
    
    
    async def download_step_2(self, filename, start_byte=0, end_byte=None):
        retries = 0
        max_retries = 9
        try:
            headers = self._headers.copy()
            if end_byte:
                headers.update({"Range": f"bytes={start_byte}-{end_byte}"})
            while True:
                async with self._session.get(self.url, headers=headers, allow_redirects=True) as r:
                        if int(r.status) < 400:
                            return await self.save_response(filename, r)
                            break
                        if retries == max_retries:
                            break
                        retries += 1
                        print(f"Retrying request due to {r.status} response, retry number: {retries}")
                        await asyncio.sleep(2**retries)
            if retries == max_retries:
                self.error = "Failed to connect to server or server closed connection unexpectedly"
                return None
        except Exception as e:
            traceback.print_exc()
            self.error = str(e)
            self.traceback = traceback.format_exc()
            return None
    
    
    async def save_response(self, filename, r):
        path = self._download_dir.joinpath(filename)
        try:
            async with aiofiles.open(path, mode='wb') as f:
                async for chunk, _ in r.content.iter_chunks():
                    if not chunk:
                        break
                    await f.write(chunk)
                    if self._progress:
                        await self.callback(len(chunk))
            return path
        except asyncio.TimeoutError:
            path.unlink()
            self.error = "Time out reached. Process is taking more than 45 mins to complete."
            return None
        except aiohttp.ClientPayloadError:
            traceback.print_exc()
            self.error = "Server did not send full file in time."
            path.unlink()
            return None
        except Exception as e:
            traceback.print_exc()
            self.error = str(e)
            self.traceback = traceback.format_exc()
            path.unlink()
            return None
    
    
    async def make_file(self):
        full_file = self._download_dir.joinpath(self._filename)
        try:
            async with aiofiles.open(full_file, 'wb') as dest:
                for l in range(self._num_of_dl_threads):
                    part_file = self._download_dir.joinpath(self._filename + f'.part{l}')
                    read_chunk = 1024*1024*10
                    async with aiofiles.open(part_file, 'rb') as fd:
                        write_chunk = await fd.read(read_chunk)
                        while write_chunk:
                            await dest.write(write_chunk)
                            write_chunk = await fd.read(read_chunk)
                    part_file.unlink()
            return full_file
        except Exception:
            traceback.print_exc()
            self.error = "File downloaded successfully, but error in combining parts"
            self.traceback = traceback.format_exc()
            return None
    
    
    async def callback(self, cur):
        try:
            self._cur += cur
            now = time.time()
            diff = now - self._start_time
            if (int(now) % 10 == 0) or (self._cur == self._size):
                await self._progress(self._cur, self._size, *self._args)
        except Exception as e:
            print(e)
            pass
