import motor.motor_asyncio

from .main import *
from config import Config
from .youtube import Youtube
from .database import Database


app = create_app()
yt = Youtube()
db = Database(
    motor.motor_asyncio.AsyncIOMotorClient(Config.DATABASE_URI)
)


async def run_bot():
    await app.start()
    
    await app.idle()
    
    await app.stop()
