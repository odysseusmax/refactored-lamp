import os
import time
import asyncio
from pathlib import Path

from pyrogram.errors import FloodWait


out_folder = Path('media/')
if not out_folder.exists():
    os.makedirs(out_folder)


def human_bytes(num, split=False):
    base = 1024.0
    sufix_list = ['B','KiB','MiB','GiB','TiB','PiB','EiB','ZiB', 'YiB']
    for unit in sufix_list:
        if abs(num) < base:
            if split:
                return round(num, 2), unit
            return f"{round(num, 2)} {unit}"
        num /= base


async def run_subprocess_cmd(cmd):
    process = await asyncio.create_subprocess_shell(cmd,
        stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
    )
    return await process.communicate()
    #print(x)


async def combine_audio_video(audio_file, video_file, filename):
    
    out_file = out_folder.joinpath(filename)
    
    cmd = f"ffmpeg -i '{video_file}' -i '{audio_file}' -c:v copy -c:a aac '{out_file}'"
    await run_subprocess_cmd(cmd)
    audio_file.unlink()
    video_file.unlink()
    return out_file


async def convert_to_mp3(audio_file, filename):
    out_file = out_folder.joinpath(filename)
    cmd = f"ffmpeg -i '{audio_file}' -acodec libmp3lame '{out_file}'"
    await run_subprocess_cmd(cmd)
    audio_file.unlink()
    return out_file


async def embed_caption(video_file, caption_files, filename):
    out_file = out_folder.joinpath(filename)
    input_files = ''
    metadata = ''
    mapings = '-map 0:v -map 0:a '
    for i, subtitle in enumerate(caption_files):
        label = subtitle.get('label')
        path = subtitle.get('path')
        input_files += f"-i '{path}' "
        mapings += f"-map {i+1} "
        metadata += f"-metadata:s:s:{i} language='{label}' "
        
    cmd = f"ffmpeg -hide_banner -i '{video_file}' {input_files} {mapings} {metadata} -c:v copy -c:a copy -c:s srt '{out_file}'"
    o, e = await run_subprocess_cmd(cmd)
    for caption_file in caption_files:
        caption_file['path'].unlink()
    caption_files.clear()
    video_file.unlink()
    if not out_file.exists():
        output = o.decode() + '\n\n' + e.decode()
        print(output)
        return False, output
    return True, out_file


async def dl_progress(cur, tot, typ, snt):
    await asyncio.sleep(1)
    percentage = cur * 100 // tot
    text = f"Downloading {typ}... {percentage} %"
    await snt.edit_message_text(text)


async def ul_progress(cur, tot, snt, start, action):
    try:
        if (int(time.time()) % 10 == 0) or (cur == tot):
            await asyncio.sleep(1)
            percentage = cur * 100 // tot
            text = f"Uploading... {percentage} %"
            await snt.message.reply_chat_action(action)
            await snt.edit_message_text(text)
    except Exception as e:
        print(e)
        pass


async def flood_shield(coro):
    while True:
        try:
            r = await coro
            return r
        except FloodWait as e:
            await asyncio.sleep(e.x)
        except Exception as e:
            print(e)
            break
