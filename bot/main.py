from pyrogram import Client

from config import Config


def create_app():
    plugins = dict(
        root="bot/plugins"
    )
    app = Client(Config.SESSION_NAME,
        bot_token = Config.BOT_TOKEN,
        api_id = Config.API_ID,
        api_hash = Config.API_HASH,
        plugins = plugins
    )
    return app
 
