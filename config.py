import os

class Config:
    
    API_ID = int(os.environ.get('API_ID'))
    API_HASH = os.environ.get('API_HASH')
    BOT_TOKEN = os.environ.get('BOT_TOKEN')
    SESSION_NAME = os.environ.get('SESSION_NAME')
    DATABASE_URI = os.environ.get('DATABASE_URI')
    LOG_CHANNEL = int(os.environ.get('LOG_CHANNEL'))
    UPLOADS_CHANNEL = int(os.environ.get('UPLOADS_CHANNEL'))
    AUTH_USERS = [int(i.strip()) for i in os.environ.get('AUTH_USERS', '').split(' ')]
